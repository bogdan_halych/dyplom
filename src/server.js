//Функція отримання масиму зареєстрованих користувачів сайту
export function getUsers() {
  const temp = JSON.parse(localStorage.users);
  return temp;
}

export function getUserByLogin(login) {
  let temp = JSON.parse(window.localStorage.users);
  let user = temp.filter((user) => user.login == login);
  return user;
}

//Показує, який юзер зараз працює на сторінці
export function showUserInHeader() {
  const users = JSON.parse(localStorage.users);
  let user = null;
  users.forEach((el) => {
    if (el.isLogin === true) {
      setTimeout(
        () => (document.querySelector(".header-right").innerHTML = el.login),
        100
      );
      user = { ...el };
    }
  });
  return user.login;
}

//Отримує масив товарів
export function getMain() {
  const temp = JSON.parse(localStorage.mainGoods);
  return temp;
}

//Обновлює масив товарів
export function setMain(arr) {
  window.localStorage.mainGoods = JSON.stringify(arr);
}

//ЛКлік у лівому верхньому куті переводить на стартову сторінку
export function onLoadHeader() {
  document.querySelector(".header-left").addEventListener("click", () => {
    location.href = "../../index.html";
  });
}

//Передає ID товару, що буде відкритий
export function sendItemIdForPage() {
  return JSON.parse(localStorage.goodsItemId);
}

//Отримуємо товар за ID
export function getGoodsById(id) {
  let temp = getMain();
  let arr = temp.filter((item) => item.id == id);
  return arr[0];
}

//отримуємо масив замовлення для конкретного клієнта
export function getClientOrders(client) {
  let temp = JSON.parse(window.localStorage.orders);
  let clientArr = temp.filter((item) => client === item.client);
  return clientArr;
}

//Перехід в кошик

export function showOrder() {
  location.href = "../order/order.html";
}

//Показує скільки товарів у кошику
export function numbersOfOrders() {
  const numberOfOrders = document.querySelector("#order-number");
  let clientNumberOrders = getClientOrders(showUserInHeader()).length;
  numberOfOrders.innerHTML = `У кошику <h2> ${clientNumberOrders} </h2> товарів`;
}

//Показує скільки замовлень треба опрацювати
export function numbersWorkingOrders() {
  let number = getWorkingOrdersForAuthor().length;
  const infoAboutWorkingOrders = document.querySelector("#working-orders");
  infoAboutWorkingOrders.innerHTML = `У вас <h2> ${number} </h2> неопрацьованих замовленя` 
}

//Перехід в замовлення
export function showWorkingOrders() {
  location.href = "../workingorders/workingorders.html";
}

//Дістаємо усі замовлення на конкретного продавця
export function getWorkingOrdersForAuthor() {
  let temp = JSON.parse(localStorage.ordersConfirm);
  let arr = temp.filter((order) => order.author === showUserInHeader());
  return arr;
}

//функція створення базових масивів в LocalStorage

export function baseArrayCreate() {
  if (!window.localStorage.orders) {
    window.localStorage.orders = JSON.stringify([]);
  }
  
  if (!window.localStorage.ordersConfirm) {
    window.localStorage.ordersConfirm = JSON.stringify([]);
  }
  
  if (!window.localStorage.allOrders) {
    window.localStorage.allOrders = JSON.stringify([]);
  }
  
  if (!window.localStorage.comments) {
    window.localStorage.comments = JSON.stringify([]);
  }
}

// Блок роботи з редагуванням користувача

const patternEditUser = {
  mail: /^[a-z._]+@[a-z._]+\.[a-z._]{1,4}$/i,
  tel: /^\+380\d{9}$/,
  pass: /^[\w+[\d\w]/ && /[A-Z]/,
}

function validateEditUser(user) {
  if (
    patternEditUser.mail.test(user.mail) &&
    patternEditUser.tel.test(user.tel) &&
    patternEditUser.pass.test(user.pass)
  ) return true
  else alert("Перевірте правильність введення даних");
  return false;
}

export function showEditForm() {
  let mainEdit = document.createElement("div");
  mainEdit.classList.add("user-edit");
  document.body.append(mainEdit);
  let title = document.createElement("h2");
  title.innerHTML = "Це Ваші дані. Ви можете їх змінити";
  let [pass, mail, tel] = [
    document.createElement("input"),
    document.createElement("input"),
    document.createElement("input")
  ];
  let [edit, cancel] = [
    document.createElement("button"),
    document.createElement("button")
  ] 
  let user = getUserByLogin(showUserInHeader())[0];
  console.log(user);
  pass.name = "pass";
  mail.name = "mail";
  tel.name = "tel";
  pass.value = user.pass;
  mail.value = user.mail;
  tel.value = user.tel;
  edit.innerHTML = "Змінити";
  cancel.innerHTML = "Відмінити";
  mainEdit.append(title, pass, mail, tel, edit, cancel);
  mainEdit.addEventListener("change", (e) => {
    let input = e.target;
    if (input.tagName === "INPUT") {
      let name = input.name;
      user[name] = input.value;
    }
  })
  edit.addEventListener("click", () => {
    if (validateEditUser(user)) {
      alert("Дані успішно змінено")
      let temp = JSON.parse(localStorage.users);
      let users = temp.filter((item) => item.login != showUserInHeader());
      users.push(user);
      localStorage.users = JSON.stringify(users);
      mainEdit.remove();
    }
  })
  cancel.addEventListener("click", () => {
    mainEdit.remove();
  })
}

// Підказка про вихід на головну сторінку

export function toolTipMainMenu(e) {
  const mainPage = document.querySelector(".header-left");
  console.log(mainPage);
  mainPage.innerHTML = "На головну";
  mainPage.onmouseout = () => {
    mainPage.innerHTML = "Це моє!";
  }
}




