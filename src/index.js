import * as data from "./server.js";

const registration = document.querySelector(".header-right li:nth-child(1)");
const logIn = document.querySelector(".header-right li:nth-child(2)");
const modal = document.querySelector(".registration");
const modalIn = document.querySelector(".log-in");
const info = document.querySelector(".info");

document.querySelector(".header-left").addEventListener("mouseover", data.toolTipMainMenu);

class CreateUser {
  constructor(firstName, secondName, mail, tel, login, pass, license) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.mail = mail;
    this.tel = tel;
    this.login = login;
    this.pass = pass;
    this.license = license;
  }
}

registration.addEventListener("click", (e) => {
  info.style.display = "none";
  modal.classList.add("active");
  if (modalIn.classList.contains("active")) {
    modalIn.classList.remove("active");
  }
});
logIn.addEventListener("click", (e) => {
  info.style.display = "none";
  modalIn.classList.add("active");
  if (modal.classList.contains("active")) {
    modal.classList.remove("active");
  }
});

// Створюємо об'єкт для валідації

const patternUser = {
  firstName: /^[А-Я,A-Z,]+[а-я,a-z,і,\S]+$/,
  secondName: /^[А-Я,A-Z,]+[а-я,a-z,і,\S]+$/,
  mail: /^[a-z._]+@[a-z._]+\.[a-z._]{1,4}$/i,
  tel: /^\+380\d{9}$/,
  login: /^\S{3,}/,
  pass: /^[\w+[\d\w]/ && /[A-Z]/,
  license: true,
  error: "Помилка введення даних",
};

// Функція валідації

let isValide = false;
let dublicate = false;
function validateUser() {
  if (
    patternUser.firstName.test(user.firstName) &&
    patternUser.secondName.test(user.secondName) &&
    patternUser.mail.test(user.mail) &&
    patternUser.tel.test(user.tel) &&
    patternUser.login.test(user.login) &&
    patternUser.pass.test(user.pass) &&
    patternUser.license === user.license
  ) {
    let users = data.getUsers();
    users.forEach((el) => {
      if (el.login === user.login) {
        alert("Користувач з таким логіном вже зареєстрований");
        dublicate = true;
        clearRegisterForm();
      }
    });
    if (!dublicate) {
      isValide = true;
      alert("Користувач створений");
      logIn.click();
      return;
    } else alert(patternUser.error);
  }
}

function onChangeValidate(pattern, key, target) {
  try {
    if (pattern.test(key)) {
      target.style.backgroundColor = "green";
      return true;
    } else target.style.backgroundColor = "red";
  } catch (e) {
    console.log("Помилкаперевірки введених даних - " + e.message);
  }
}

// Даний блок необхідний для автоматизації створення нового
// юзера при натисканні на "Реєстрація"

let user = null;
const isModal = () => {
  if (modal.classList.contains("active")) {
    user = new CreateUser();
    clearInterval(changeModal);
  }
};
let changeModal = setInterval(isModal, 1000);

const registrationForm = document.querySelector("#registration");
registrationForm.addEventListener("change", (e) => {
  let target = e.target;
  const name = target.name;
  user[name] = target.type === "checkbox" ? target.checked : target.value;
  onChangeValidate(patternUser[target.name], user[name], target);
});

if (!window.localStorage.users) {
  window.localStorage.users = JSON.stringify([]);
}

function saveUser() {
  let temp = JSON.parse(window.localStorage.users);
  temp.push({ ...user, isLogin: false });
  window.localStorage.users = JSON.stringify(temp);
}

//Очищаємо поля після введення

function clearRegisterForm() {
  const inputs = document.querySelectorAll("#registration>input:not(#licence)");
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].value = "";
    inputs[i].style.backgroundColor = "";
  }
  document.querySelector("#licence").checked = false;
}
clearRegisterForm();

const registrationBtn = document.querySelector("#registration-btn");
registrationBtn.addEventListener("click", () => {
  validateUser();
  if (isValide) {
    saveUser();
    clearRegisterForm();
  }
});

// Даний блок забезпечує вхід на сторінку Юзера

const loginForm = document.querySelector("#logIn");
const logInBtn = document.querySelector("#login-btn");
const [userLogin, userPass] = [
  document.querySelector("#log"),
  document.querySelector("#pass"),
];
logInBtn.addEventListener("click", () => {
  const users = data.getUsers();
  let isUser = false;
  for (let i = 0; i < users.length; i++) {
    if (
      users[i].login === userLogin.value &&
      users[i].pass === userPass.value
    ) {
      users[i].isLogin = true;
      location.href = "./pages/main/main.html";
      isUser = true;
    }
  }
  window.localStorage.users = JSON.stringify(users);
  if (!isUser) alert("Користувача з таким логіном і паролем не знайдено");
});

//При кожному перезавантаженні сторінки значення isLogin = false;
//На main.html та інших значення активного користувача зберігається

window.onload = () => {
  let users = data.getUsers();
  for (let i = 0; i < users.length; i++) {
    users[i].isLogin = false;
  }
  window.localStorage.users = JSON.stringify(users);
};

// Блок INFO

function getColor() {
  return `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
}
function changeTitle() {
  document.querySelector(".info>h2").style.color = getColor();
}
setInterval(changeTitle, 1000);

let loadTest = document.querySelector(".load-test");

function loadDataStartTest() {
  localStorage.users = JSON.stringify([
    {
      firstName: "Богдан",
      isLogin: false,
      license: true,
      login: "bogdanhal",
      mail: "bogdan_halych@ukr.net",
      pass: "Bb12345",
      secondName: "Зіньковський",
      tel: "+380508200837",
    },
    {
      firstName: "Максим",
      isLogin: false,
      license: true,
      login: "max",
      mail: "bogdan_halych@ukr.net",
      pass: "Mm12345",
      secondName: "Зіньковський",
      tel: "+380508200837",
    },
    {
      firstName: "Дем'ян",
      isLogin: false,
      license: true,
      login: "demjan",
      mail: "bogdan_halych@ukr.net",
      pass: "Dd12345",
      secondName: "Зіньковський",
      tel: "+380508200837",
    },
  ]);
  localStorage.mainGoods = JSON.stringify([
    {
      author: "bogdanhal",
      categories: "notebooks",
      description: "Asus X1000S",
      id: 1662219959259,
      img: "../../img/goods/note1.png",
      name: "Asus X1000S",
      price: "2000",
    },
    {
      author: "bogdanhal",
      categories: "notebooks",
      description: "Ноутбук HP G655",
      id: 1662220006656,
      img: "../../img/goods/note2.png",
      name: "Ноутбук HP G655",
      price: "1200",
    },
    {
      author: "bogdanhal",
      categories: "notebooks",
      description: "Ноутбук HP G650",
      id: 1662220070869,
      img: "../../img/goods/note2.png",
      name: "Ноутбук HP G650",
      price: "10000",
    },
    {
      author: "bogdanhal",
      categories: "notebooks",
      description: "MSI M200 DXS450vd",
      id: 1662220085743,
      img: "../../img/goods/note4.png",
      name: "MSI M200 DXS450vd",
      price: "45000",
    },
    {
      author: "bogdanhal",
      categories: "notebookComponents",
      description: "DiMM 4Gb DDR4",
      id: 1662220100227,
      img: "../../img/goods/dimm.png",
      name: "DiMM 4Gb DDR4",
      price: "600",
    },
    {
      author: "bogdanhal",
      categories: "notebookComponents",
      description: "DiMM 8Gb DDR4",
      id: 1662220113806,
      img: "../../img/goods/dimm.png",
      name: "DiMM 8Gb DDR4",
      price: "11000",
    },
    {
      author: "max",
      categories: "notebookComponents",
      description: "SSD 120 GoodRAM",
      id: 1662220241423,
      img: "../../img/goods/hdd.png",
      name: "SSD 120 GoodRAM",
      price: "700",
    },
    {
      author: "max",
      categories: "notebookComponents",
      description: "SSD 240 GoodRAM",
      id: 1662220265908,
      img: "../../img/goods/hdd.png",
      name: "SSD 240 GoodRAM",
      price: "1250",
    },
    {
      author: "max",
      categories: "monitors",
      description: "Samsung 27'FullHD",
      id: 1662220284444,
      img: "../../img/goods/monitor3.png",
      name: "Samsung 27'FullHD",
      price: "1600",
    },
    {
      author: "max",
      categories: "monitors",
      description: "Asus 24' Super",
      id: 1662220312485,
      img: "../../img/goods/monitor5.png",
      name: "Asus 24' Super",
      price: "4500",
    },
    {
      author: "demjan",
      categories: "monitors",
      description: "Samsung 24' Super",
      id: 1662220467747,
      img: "../../img/goods/monitor1.png",
      name: "Samsung 24' Super",
      price: "7000",
    },
    {
      author: "demjan",
      categories: "printers",
      description: "Canon MFP 3600",
      id: 1662220491819,
      img: "../../img/goods/printer1.png",
      name: "Canon MFP 3600",
      price: "12000",
    },
    {
      author: "demjan",
      categories: "printers",
      description: "Canon LBP2900",
      id: 1662220510387,
      img: "../../img/goods/printer2.png",
      name: "Canon LBP2900",
      price: "4500",
    },
    {
      author: "demjan",
      categories: "other",
      description: "VC ATI Radeon RX 580",
      id: 1662220533276,
      img: "../../img/goods/videocart.png",
      name: "VC ATI Radeon RX 580",
      price: "75000",
    },
    {
      author: "bogdanhal",
      categories: "other",
      description: "Something10",
      id: 1662220771250,
      img: "../../img/goods/monitor5.png",
      name: "Something10",
      price: "100",
    },
  ]);
  localStorage.comments = JSON.stringify([
    {
      uthor: "bogdanhal",
      client: "max",
      date: "03.09.2022",
      text: "Хочеться більше характеристик",
    },
    {
      author: "demjan",
      client: "max",
      date: "03.09.2022",
      text: "Який ресурс друку однієї заправки",
    },
  ]);
  localStorage.allOrders = JSON.stringify([
    {
      author: "bogdanhal",
      categories: "notebooks",
      client: "max",
      date: "03.09.2022",
      description: "Asus X1000S",
      id: 1662219959259,
      img: "../../img/goods/note1.png",
      isConfirmAuthor: true,
      name: "Asus X1000S",
      price: "2000",
      time: "18:52:56",
    },
    {
      author: "bogdanhal",
      categories: "notebooks",
      client: "max",
      date: "03.09.2022",
      description: "MSI M200 DXS450vd",
      id: 1662220085743,
      img: "../../img/goods/note4.png",
      isConfirmAuthor: false,
      name: "MSI M200 DXS450vd",
      price: "45000",
      time: "18:52:56",
    },
    {
      author: "demjan",
      categories: "printers",
      client: "max",
      date: "03.09.2022",
      description: "Canon LBP2900",
      id: 1662220510387,
      img: "../../img/goods/printer2.png",
      isConfirmAuthor: true,
      name: "Canon LBP2900",
      price: "4500",
      time: "18:57:45",
    },
  ]);
}

loadTest.addEventListener("click", loadDataStartTest);