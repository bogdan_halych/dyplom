import * as data from "../../src/server.js";

data.baseArrayCreate();
data.showUserInHeader();
data.onLoadHeader();
data.numbersOfOrders();
data.numbersWorkingOrders();

document.querySelector(".header-left").addEventListener("mouseover", data.toolTipMainMenu);
document.querySelector(".header-right-center").addEventListener("click", data.showOrder);
document.querySelector(".header-left-center").addEventListener("click", data.showWorkingOrders);
document.querySelector(".header-right").addEventListener("click", data.showEditForm);

function getCommentsForMe() {
  let temp = JSON.parse(localStorage.comments);
  let comments = temp.filter((item) => item.author === data.showUserInHeader());
  return comments;
};
const comments = getCommentsForMe();

let commentsInThePage = 10; //Задаємо кількісь елементів на сторінці
let numberOfPages = Math.floor(comments.length / commentsInThePage + 1); //Вираховуємо кількісь сторінок
console.log(numberOfPages);

let historyBox = document.querySelector(".history-box"); //Блок інформації
let pageList = document.querySelector(".page-number"); //Блок сторінок

//Створення нумерації сторінок
for (let i = 0; i < numberOfPages; i++) {
  let li = document.createElement("li");
  li.innerHTML = i + 1;
  pageList.append(li);
}

//Блок виводу інформації

let list = document.querySelectorAll(".page-number>li");
let listArr = [...list];
console.log(listArr);
for (let li of listArr) {
  li.addEventListener("click", (e) => {
    let items = [...document.querySelectorAll(".item")];
    console.log(items);
    if (items.length > 0) {
      for (let item of items) {
        item.remove();
      }
    }
    let number = +li.innerHTML;
    let start = (number - 1) * commentsInThePage;
    let end = start + commentsInThePage;
    let orders = comments.slice(start, end);
    console.log(orders);
    orders.forEach((el) => {
      let item = document.createElement("div");
      item.classList.add("item");
      let [client, date, text] = [
        document.createElement("div"),
        document.createElement("div"),
        document.createElement("div"),
      ];
      client.classList.add("author");
      date.classList.add("date");
      text.classList.add("comment")
      client.innerHTML = el.client;
      date.innerHTML = el.date;
      text.innerHTML = el.text;
      item.append(client, date, text);
      historyBox.prepend(item);
    });
  });
}

window.onload = function () {
  if (comments.length > 0) {
    [...document.querySelectorAll(".page-number>li")][0].click();
  }
}