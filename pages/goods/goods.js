import * as data from "../../src/server.js";

data.baseArrayCreate();
data.showUserInHeader();
data.onLoadHeader();
data.numbersOfOrders();
data.numbersWorkingOrders();

document.querySelector(".header-right-center").addEventListener("click", data.showOrder);
document.querySelector(".header-left-center").addEventListener("click", data.showWorkingOrders);
document.querySelector(".header-right").addEventListener("click", data.showEditForm);

let itemId = data.sendItemIdForPage();
const goodsObject = data.getGoodsById(itemId);

document.querySelector("#comment").value = "";
//Функція знаходить за автором товару його дані для подальшого відображення.
function getAuthorDataByAuthor() {
  let temp = data.getUsers();
  let arr = temp.filter((author) => author.login === goodsObject.author);
  let author = arr[0];
  return author;
}
let thisAuthor = getAuthorDataByAuthor();

function showPageGoods() {
  document.querySelector(".goods-title").innerHTML = goodsObject.name;
  document.querySelector(
    ".goods-img"
  ).style.backgroundImage = `url('${goodsObject.img}')`;
  document.querySelector(".goods-description").innerHTML =
    goodsObject.description;
  document.querySelector(".aside-title").innerHTML +=
    thisAuthor.firstName + " " + thisAuthor.secondName;
  document.querySelector(".user-list>li:nth-child(1)").innerHTML +=
    "<p>" + thisAuthor.login + "</p>";
  document.querySelector(".user-list>li:nth-child(2)").innerHTML +=
    "<p>" + thisAuthor.mail + "</p>";
  document.querySelector(".user-list>li:nth-child(3)").innerHTML +=
    "<p>" + thisAuthor.tel + "</p>";
}

window.onload = showPageGoods();

//Робота з Кошиком


class Order {
  constructor(client, goodsId) {
    this.client = client;
    this.goodsId = goodsId;
  }
  setOrder() {
    let temp = JSON.parse(localStorage.orders);
    temp.push({ client: this.client, goodsId: this.goodsId });
    window.localStorage.orders = JSON.stringify(temp);
  }
}

const sendOrder = document.querySelector("#send-order");//Кнопка для збереження у кошик

sendOrder.addEventListener("click", () => {
  const order = new Order(data.showUserInHeader(), goodsObject.id);
  order.setOrder();
  alert("Замовлення добавлено у кошик");
  location.href = "../../pages/shop/shop.html";
});

class Comment {
  constructor(client, date, text, author) {
    this.client = client;
    this.date = date;
    this.text = text;
    this.author = author;
  }
}

const sendComment = document.querySelector(".goods-btn");
sendComment.addEventListener("click", () => {
  let temp = JSON.parse(localStorage.comments);
  let comment = new Comment(
    data.showUserInHeader(),
    new Date().toLocaleDateString(),
    document.querySelector("#comment").value,
    goodsObject.author
  )
  temp.push(comment);
  console.log(temp)
  localStorage.comments = JSON.stringify(temp);
  document.querySelector("#comment").value = "";
})




