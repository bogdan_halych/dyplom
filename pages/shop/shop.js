import * as data from "../../src/server.js";

data.baseArrayCreate();
data.showUserInHeader();
data.onLoadHeader();
data.numbersOfOrders();
data.numbersWorkingOrders();

document.querySelector(".header-left").addEventListener("mouseover", data.toolTipMainMenu);
document
  .querySelector(".header-right-center")
  .addEventListener("click", data.showOrder);
document
  .querySelector(".header-left-center")
  .addEventListener("click", data.showWorkingOrders);
document.querySelector(".header-right").addEventListener("click", data.showEditForm);

const goodsBox = document.querySelector(".goods-box");
const goodsEdit = document.querySelector("#goods-edit-btn");

//Відкриваємо/Закриваємо вікно додавання товару

const closeAddGoods = document.querySelector("#close-add");
const openAddGoods = document.querySelector(".open-add");

closeAddGoods.addEventListener("click", () => {
  [...goodsAddForm.elements].map((el) => (el.value = ""));
  document.querySelector(".add-goods-modal").style.display = "none";
});
openAddGoods.addEventListener("click", () => {
  goodsAddBtn.style.display = "block"; //чергуємо видимисть вікна додавання і редагування
  goodsEdit.style.display = "none";
  document.querySelector(".add-goods-modal").style.display = "block";
});

const chooseImg = document.querySelector("#file");

//Блок роботи зі створення об'єкта товару

class NewGoods {
  constructor(id, author, categories, img, name, description, price) {
    this.id = id;
    this.author = author;
    this.categories = categories;
    this.img = img;
    this.name = name;
    this.description = description;
    this.price = price;
  }
  getDataFormGoods() {
    this.categories = document.querySelector("#categories").value;
    this.img = chooseImg.value;
    this.name = document.querySelector("#goods-name").value;
    this.description = document.querySelector("#goods-description").value;
    this.price = document.querySelector("#goods-price").value;
  }
}

let goods = new NewGoods(Date.now(), data.showUserInHeader());

const goodsAddForm = document.querySelector(".add-goods-form");
goodsAddForm.addEventListener("change", (e) => {
  let target = e.target;
  let name = target.name;
  goods[name] = target.value;
  console.log(name);
});

//Створюємо функцію додавання товарів на LocalStorage
//Шлях до файлу з img не коректний тому мусимо його підкоректувати

function correctImg() {
  if (goods.img) {
    let temp = Array.from(goods.img);
    temp.splice(0, 12);
    temp.unshift("../../img/goods/");
    let img = temp.join("");
    goods.img = img;
  }
}
//setInterval(correctImg, 5000);

function validateGoods() {
  const patternName = /[\s\S\w]{3,20}/gi;
  const patternDescription = /[\s\S\w]{10,300}/gi;
  const patternPrice = /\d+/;
  if (
    patternName.test(goods.name) &&
    patternDescription.test(goods.description) &&
    patternPrice.test(goods.price)
  )
    return true;
  else {
    alert("Заповніть усі поля належним чином");
  }
}

function saveGoods() {
  let temp = data.getMain();
  temp.push(goods);
  window.localStorage.mainGoods = JSON.stringify(temp);
}

const goodsAddBtn = document.querySelector("#goods-add-btn");
goodsAddBtn.addEventListener("click", () => {
  goods.getDataFormGoods();
  correctImg();
  if (validateGoods()) {
    saveGoods();
    closeAddGoods.click();
    location.reload();
  }
});

//setInterval(() => console.log(goods), 10000);

//Створюємо об'єкт в LocalStorage для збереження усіх товарів

if (!window.localStorage.mainGoods) {
  window.localStorage.mainGoods = JSON.stringify([]);
}

// !!! ФУНКЦІЯ, ЯКА ПРИ ЗАВАНТАЖЕННІ ФОРМУЄ БЛОК ТОВАРУ, завантажуючи з LocalStorage

//Створимо попередньо змінну, яка буде визначати чи відображаються усі товари
//чи тільки заданого користувача

let pageGoodsArr = null; //Діючий масив товару на сторінці
//Змінюється в залежності від відображення попередніх фільтрів.active
//Буде використовуватись для передачі даних уже зроблених фільтрацій у наступні

let isSelectedUserGoods = false; //Шніціалізує вхід(для всіх/конкретний користувач)

const sortBy = document.querySelector("#sort-by"); //Ініціалізація об'єкту для сортування
const search = document.querySelector(".search-goods"); //Ініціалізація об'єкту для пошуку
let orderInThePage = 12; //Задаємо кількісь елементів на сторінці
let pageList = document.querySelector(".page-number"); //Блок сторінок
function clearPageNumber() {
  pageList.innerHTML = "";
}

function showAllGoodsItems(items = data.getMain()) {
  pageGoodsArr = [...items];
  let numberOfPages = Math.floor(pageGoodsArr.length / orderInThePage + 1); //Вираховуємо кількісь сторінок
  for (let i = 0; i < numberOfPages; i++) {
    let li = document.createElement("li");
    li.innerHTML = i + 1;
    pageList.append(li);
  }
  const goodsBox = document.querySelector(".goods-box");
  goodsBox.innerHTML = "";
  let list = document.querySelectorAll(".page-number>li");
  let listArr = [...list];
  for (let li of listArr) {
    li.addEventListener("click", (e) => {
      let items = [...document.querySelectorAll(".goods-item")];
      if (items.length > 0) {
        for (let item of items) {
          item.remove();
        }
      }
      let number = +li.innerHTML;
      let start = (number - 1) * orderInThePage;
      let end = start + orderInThePage;
      let orders = pageGoodsArr.slice(start, end);
      console.log(orders);
      orders.forEach((item) => {
        let goodsItem = document.createElement("div");
        goodsItem.classList.add("goods-item");
        goodsItem.setAttribute("data-id", item.id);
        let goodsImg = document.createElement("div");
        goodsImg.style.backgroundImage = `url("${item.img}")`;
        goodsImg.setAttribute("data-id", item.id);
        goodsImg.classList.add("goods-img");
        let goodsName = document.createElement("div");
        goodsName.classList.add("goods-name");
        goodsName.setAttribute("data-id", item.id);
        goodsName.innerHTML = item.name;
        let goodsPrice = document.createElement("div");
        goodsPrice.setAttribute("data-id", item.id);
        goodsPrice.classList.add("goods-price");
        goodsPrice.innerHTML = item.price + "грн.";
        goodsBox.append(goodsItem);
        goodsItem.append(goodsImg, goodsName, goodsPrice);
      });
    });
  }
  [...document.querySelectorAll(".page-number>li")][0].click();
  isSelectedUserGoods = false;
  if (pageGoodsArr.length === 0) {
    goodsBox.innerHTML = `<h1>За даним запитом не знайдено нічого</h1>`;
  }
}
showAllGoodsItems();

const BtnAllGoods = document.querySelector("#btn-all-goods");
BtnAllGoods.addEventListener("click", () => {
  clearPageNumber();
  showAllGoodsItems();
});

//Блок для відображення товарів залогіненого користувача

function showMyGoodsItems(
  login = data.showUserInHeader(),
  goodsArr = data.getMain()
) {
  const myGoods = goodsArr.filter((el) => el.author === login);
  pageGoodsArr = [...myGoods];
  let numberOfPages = Math.floor(pageGoodsArr.length / orderInThePage + 1); //Вираховуємо кількісь сторінок
  for (let i = 0; i < numberOfPages; i++) {
    let li = document.createElement("li");
    li.innerHTML = i + 1;
    pageList.append(li);
  }
  const goodsBox = document.querySelector(".goods-box");
  goodsBox.innerHTML = "";
  let list = document.querySelectorAll(".page-number>li");
  let listArr = [...list];
  console.log(listArr);
  for (let li of listArr) {
    li.addEventListener("click", (e) => {
      let items = [...document.querySelectorAll(".goods-item")];
      console.log(items);
      if (items.length > 0) {
        for (let item of items) {
          item.remove();
        }
      }
      let number = +li.innerHTML;
      let start = (number - 1) * orderInThePage;
      let end = start + orderInThePage;
      let orders = pageGoodsArr.slice(start, end);
      console.log(orders);
      orders.forEach((item) => {
        let goodsItem = document.createElement("div");
        goodsItem.classList.add("goods-item");
        goodsItem.setAttribute("data-id", item.id);
        let goodsImg = document.createElement("div");
        goodsImg.style.backgroundImage = `url("${item.img}")`;
        goodsImg.setAttribute("data-id", item.id);
        goodsImg.classList.add("goods-img");
        let goodsName = document.createElement("div");
        goodsName.classList.add("goods-name");
        goodsName.setAttribute("data-id", item.id);
        goodsName.innerHTML = item.name;
        let goodsPrice = document.createElement("div");
        goodsPrice.setAttribute("data-id", item.id);
        goodsPrice.classList.add("goods-price");
        goodsPrice.innerHTML = item.price + "грн.";
        let goodsEditBtn = document.createElement("buutton");
        goodsEditBtn.classList.add("goods-edit-btn");
        goodsEditBtn.setAttribute("data-id", item.id);
        goodsEditBtn.innerHTML = "Редагувати";
        let goodsDeleteBtn = document.createElement("buutton");
        goodsDeleteBtn.classList.add("goods-delete-btn");
        goodsDeleteBtn.innerHTML = "Видалити";
        goodsDeleteBtn.setAttribute("data-id", item.id);
        goodsBox.append(goodsItem);
        goodsItem.append(
          goodsImg,
          goodsName,
          goodsPrice,
          goodsEditBtn,
          goodsDeleteBtn
        );
      });
    });
  }
  [...document.querySelectorAll(".page-number>li")][0].click();
  isSelectedUserGoods = true;
  if (pageGoodsArr.length === 0) {
    goodsBox.innerHTML = `<h1>За даним запитом не знайдено нічого</h1>`;
  }
}

//Функція затирає створені li-шки для відображення інформації при click;
const BtnMyGoods = document.querySelector("#btn-my-goods");
BtnMyGoods.addEventListener("click", () => {
  clearPageNumber();
  showMyGoodsItems();
});

//Блок Організації пошуку для всіх та конкретного

function getSearchedArray(temp = data.getMain()) {
  let res = temp.filter((item) =>
    item.name.toLowerCase().includes(search.value.toLowerCase())
  );
  pageGoodsArr = [...res];
  console.log(pageGoodsArr);
  return res;
}

const goodsBtnSearch = document.querySelector("#btn-search");
goodsBtnSearch.addEventListener("click", () => {
  [...document.querySelectorAll(".page-number>li")][0].click();
  clearPageNumber();
  if (isSelectedUserGoods) {
    showMyGoodsItems(data.showUserInHeader(), getSearchedArray());
  } else showAllGoodsItems(getSearchedArray());
  search.value = "";
});

//Блок роботи над сортуванням

function sortPriceIncrement() {
  let arr = pageGoodsArr.sort((a, b) => a.price - b.price);
  if (isSelectedUserGoods) {
    showMyGoodsItems(data.showUserInHeader(), arr);
  } else showAllGoodsItems(arr);
  pageGoodsArr = [...arr];
}

function sortPriceDecrement() {
  let arr = pageGoodsArr.sort((a, b) => b.price - a.price);
  if (isSelectedUserGoods) {
    showMyGoodsItems(data.showUserInHeader(), arr);
  } else showAllGoodsItems(arr);
  pageGoodsArr = [...arr];
}

function sortByName() {
  console.log("Before");
  console.log(pageGoodsArr);
  let arr = pageGoodsArr.sort((a, b) => (a.name > b.name ? 1 : -1));
  console.log("After");
  console.log(arr);
  if (isSelectedUserGoods) {
    showMyGoodsItems(data.showUserInHeader(), arr);
  } else showAllGoodsItems(arr);
  pageGoodsArr = [...arr];
}

sortBy.addEventListener("change", () => {
  clearPageNumber();
  if (sortBy.value === "priceInc") sortPriceIncrement();
  if (sortBy.value === "priceDec") sortPriceDecrement();
  if (sortBy.value === "name") sortByName();
  [...document.querySelectorAll(".page-number>li")][0].click();
});

// Блок редагування товару

goodsBox.addEventListener("click", (e) => {
  if (e.target.classList.contains("goods-edit-btn")) {
    document.querySelector(".add-goods-modal").style.display = "block";
    let btn = e.target;
    let items = data.getMain();
    let myItem = items.filter((item, i) => item.id == btn.dataset.id);
    document.querySelector("#categories").value = myItem[0].categories;
    document.querySelector("#goods-name").value = myItem[0].name;
    document.querySelector("#goods-description").value = myItem[0].description;
    document.querySelector("#goods-price").value = myItem[0].price;
    goodsAddBtn.style.display = "none"; //чергуємо видимисть вікна додавання і редагування
    goodsEdit.style.display = "block";
    console.log(myItem[0].img);
    console.log(goods.img);
    goodsEdit.addEventListener("click", () => {
      goods.getDataFormGoods();
      correctImg();
      if (!goods.img) {
        goods.img = myItem[0].img;
      }
      console.log(goods);
      if (validateGoods()) {
        let arr = items.filter((item) => btn.dataset.id != item.id);
        console.log(arr);
        data.setMain(arr);
        saveGoods();
        closeAddGoods.click();
        location.reload();
      }
    });
  }
});

//Видалення товару

goodsBox.addEventListener("click", (e) => {
  if (e.target.classList.contains("goods-delete-btn")) {
    let btn = e.target;
    let temp = data.getMain();
    console.log(temp);
    const arr = temp.filter((item) => btn.dataset.id != item.id);
    console.log(arr);
    data.setMain(arr);
    location.reload();
  }
});

//Перехід на сторінку товару з передачею на сервер id товару

export let itemId;

goodsBox.addEventListener("click", (e) => {
  if (e.target.closest(".goods-item")) {
    let item = e.target;
    if (
      item.classList.contains("goods-delete-btn") ||
      item.classList.contains("goods-edit-btn")
    )
      return;
    const itemId = item.dataset.id;
    window.localStorage.goodsItemId = JSON.stringify(itemId);
    location.href = "../goods/goods.html";
  }
});

//Aside

let lies = [...document.querySelectorAll("#categories>*")].map((item) => {
  return {
    text: item.textContent,
    categories: item.value,
  };
});
let liContent = lies.slice(1);
const ul = document.querySelector(".categories-list");

console.log(liContent);
liContent.forEach((item) => {
  let li = document.createElement("li");
  li.innerHTML = item.text;
  li.setAttribute("data-categories", item.categories);
  ul.append(li);
});

// function sortPriceDecrement() {
//   let arr = pageGoodsArr.sort((a, b) => b.price - a.price);
//   if (isSelectedUserGoods) {
//     showMyGoodsItems(data.showUserInHeader(), arr);
//   } else showAllGoodsItems(arr);
//   pageGoodsArr = [...arr];
// }

function showCategories(categories) {
  pageGoodsArr = [...data.getMain()];
  let arr = pageGoodsArr.filter((item) => item.categories === categories);
  if (isSelectedUserGoods) {
    showMyGoodsItems(data.showUserInHeader(), arr);
  } else showAllGoodsItems(arr);
}

ul.addEventListener("click", (e) => {
  if (e.target.tagName === "LI") {
    console.log(pageGoodsArr);
    clearPageNumber();
    let li = e.target;
    showCategories(li.dataset.categories);
  }
});

// ________________________________________________

//Тут записуємо
// window.localStorage.clone = JSON.stringify([]);
// let arr = JSON.parse(window.localStorage.mainGoods);
// console.log(arr)
// window.localStorage.clone = JSON.stringify(arr);

//Тут витягуєм
// let temp = JSON.parse(window.localStorage.clone);
// window.localStorage.mainGoods = JSON.stringify(temp);
