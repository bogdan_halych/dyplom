import * as data from "../../src/server.js";

data.baseArrayCreate();
data.showUserInHeader();
data.onLoadHeader();
data.numbersOfOrders();
data.numbersWorkingOrders();

document.querySelector(".header-left").addEventListener("mouseover", data.toolTipMainMenu);
document.querySelector(".header-right-center").addEventListener("click", data.showOrder);  
document.querySelector(".header-left-center").addEventListener("click", data.showWorkingOrders);
document.querySelector(".header-right").addEventListener("click", data.showEditForm);

//Отримуємо наші замовлення для підтвурдження
let workingOrder = data.getWorkingOrdersForAuthor();
console.log(workingOrder);
const orderWorkBox = document.querySelector(".work-order-box");


//Виводимо замовлення якщо вони є на сторінку 
function showOrderToPage() {
  if (workingOrder.length === 0) {
    document.querySelector("#work-order-title").innerHTML = "Нових замовлень немає";
  } else document.querySelector("#work-order-title").innerHTML = `У вас ${workingOrder.length} замовлення. Зв'яжіться з клієнтом та підтвердіть або скасуйте замовлення`;
  workingOrder.forEach((el, index) => {
    let item = document.createElement("div");
    item.classList.add("work-order-item");
    let number = document.createElement("div");
    number.innerHTML = index + 1;
    let client = document.createElement("div");
    let userArr = data.getUserByLogin(el.client);
    let user = userArr[0];
    console.log(user);
    client.innerHTML = user.firstName + "<br>" + user.tel + "<br>" + user.mail;
    let date = document.createElement("div");
    date.innerHTML = el.date;
    let name = document.createElement("div");
    name.classList.add("work-order-name");
    name.setAttribute("data-id", el.id);
    name.innerHTML = el.name;
    let time = document.createElement("div");
    time.innerHTML = el.time;
    let id = document.createElement("div");
    id.innerHTML = el.id;
    let price = document.createElement("div");
    price.innerHTML = el.price;
    let div = document.createElement("div");
    div.classList.add("btn-item-work");
    let btn1 = document.createElement("button");
    btn1.classList.add("work-order-btn-yes");
    btn1.setAttribute("data-id", el.id);
    btn1.innerHTML = "Підтвердити";
    let btn2 = document.createElement("button");
    btn2.classList.add("work-order-btn-no");
    btn2.setAttribute("data-id", el.id);
    btn2.innerHTML = "Скасувати";
    div.append(btn1, btn2);
    item.append(number, client, date, time, name, id, price, div);
    orderWorkBox.append(item);
  })
}

window.onload = showOrderToPage();

//Робимо можливість подивитись на товар сліком по назві
orderWorkBox.addEventListener("click", (e) => {
  if (e.target.closest(".work-order-name")) {
    let item = e.target;
    const itemId = item.dataset.id;
    window.localStorage.goodsItemId = JSON.stringify(itemId);
    location.href = "../goods/goods.html";
  }
});

//Створюємо функцію видалення замовлення по ID.
//Буде застосовуватись при натисканні на кнопки "Підтвердити" і "Скасувати"
//бо вона видаляє з основного масиву замовлень.
//Перед видаленням дані будуть записуватись у новий масив AllOrders за
// новим логічним ключем isConfirmAuthor(true/false)

function deleteOrder(id) {
  let temp = JSON.parse(localStorage.ordersConfirm);
  console.log(temp);
  let arr = temp.filter((item) => item.id != id);
  console.log(arr);
  window.localStorage.ordersConfirm = JSON.stringify(arr);
}

//Натискаємо на кнопки:
orderWorkBox.addEventListener("click", (e) => {
  let allArr = [];
  let btn = e.target;
  if (btn.closest(".work-order-btn-yes")) {
    allArr = workingOrder.map((item) => {
      if (btn.dataset.id == item.id) return {
        ...item,
        isConfirmAuthor: true
      }
      else return "";
    })
  }
  if (btn.closest(".work-order-btn-no")) {
    allArr = workingOrder.map((item) => {
      if (btn.dataset.id == item.id) return {
        ...item,
        isConfirmAuthor: false
      }
      else return "";
    })
  }
  let item = allArr.filter((el) => el != "");
  let order = item[0];
  deleteOrder(btn.dataset.id);
  let temp = JSON.parse(window.localStorage.allOrders);
  temp.push(order);
  window.localStorage.allOrders = JSON.stringify(temp);
  location.reload();
})

//Натискання кнопки для всіх
document.querySelector("#work-yes").addEventListener("click", (e) => {
  let allArr = [];
  allArr = workingOrder.map((item) => { return { ...item, isConfirmAuthor: true } });
  let temp = JSON.parse(window.localStorage.allOrders);
  let res = [...temp, ...allArr];
  window.localStorage.allOrders = JSON.stringify(res);
  workingOrder.forEach((el) => deleteOrder(el.id));
  location.reload();
})
document.querySelector("#work-no").addEventListener("click", (e) => {
  let allArr = [];
  allArr = workingOrder.map((item) => { return { ...item, isConfirmAuthor: false } });
  let temp = JSON.parse(window.localStorage.allOrders);
  let res = [...temp, ...allArr];
  window.localStorage.allOrders = JSON.stringify(res);
  workingOrder.forEach((el) => deleteOrder(el.id));
  location.reload();
})
