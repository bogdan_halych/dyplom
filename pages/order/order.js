import * as data from "../../src/server.js";

data.baseArrayCreate();
data.showUserInHeader();
data.onLoadHeader();
data.numbersOfOrders();
data.numbersWorkingOrders();

document.querySelector(".header-left").addEventListener("mouseover", data.toolTipMainMenu);
document
  .querySelector(".header-right-center")
  .addEventListener("click", data.showOrder);
document
  .querySelector(".header-left-center")
  .addEventListener("click", data.showWorkingOrders);
document
  .querySelector(".header-right")
  .addEventListener("click", data.showEditForm);

//Отримуємо масив товарів у кошику для даного користувача

let myOrders = data.getClientOrders(data.showUserInHeader()); //масив (клієнт + ID)
let myOrdersId = myOrders.map((el) => el.goodsId); //масив ID

function getOrderArrayGoods() {
  let temp = data.getMain();
  let myArr = [];
  for (let i = 0; i < temp.length; i++) {
    for (let j = 0; j < myOrdersId.length; j++) {
      if (temp[i].id == myOrdersId[j]) {
        myArr.push(temp[i]);
      }
    }
  }
  return myArr;
}

let myOrdersArray = getOrderArrayGoods(); //масив товарів у кошику.

//Створюємо функцію відображення товарів

const orderBox = document.querySelector(".order-box");

function showOrderGoods() {
  myOrdersArray.forEach((item, index) => {
    let orderItem = document.createElement("div");
    orderItem.classList.add("order-item");
    let orderNumber = document.createElement("div");
    orderNumber.classList.add("order-number");
    orderNumber.innerHTML = index + 1;
    let orderImg = document.createElement("div");
    orderImg.classList.add("order-img");
    orderImg.style.backgroundImage = `url("${item.img}")`;
    orderImg.setAttribute("data-id", item.id);
    let orderName = document.createElement("div");
    orderName.classList.add("order-name");
    orderName.innerHTML = item.name;
    let orderDescription = document.createElement("div");
    orderDescription.classList.add("order-description");
    orderDescription.innerHTML = item.description;
    let orderPrise = document.createElement("div");
    orderPrise.classList.add("order-price");
    orderPrise.innerHTML = item.price;
    let orderBtn = document.createElement("button");
    orderBtn.classList.add("order-btn");
    orderBtn.innerHTML = "Видалити";
    orderBtn.setAttribute("data-id", item.id);
    orderItem.append(
      orderNumber,
      orderImg,
      orderName,
      orderDescription,
      orderPrise,
      orderBtn
    );
    orderBox.append(orderItem);
  });
  let priseArr = document.querySelectorAll(".order-price");
  let res = [...priseArr].reduce((sum, current) => sum + parseInt(current.innerHTML), 0);
  document.querySelector(".order-sum").innerHTML = "Всього на суму: " + res + " грн.";
}

window.onload = showOrderGoods();

//Організовуєм видалення з кошика

function deleteOrder(id) {
  let temp = myOrders.filter((item) => item.goodsId == id);//знаходимо у нашому кошику цей товар
  let order = temp[0];
  let allOrders = JSON.parse(localStorage.orders);//дістаєто товари зусіх кошиків
  for (let i = 0; i < allOrders.length; i++) {
    if (allOrders[i].client == order.client && allOrders[i].goodsId == order.goodsId) {
      allOrders.splice(i, 1);
    }
  } //Перевіряли на рівність ID та client. Щоб повністю не видалити елемент бо кілька клієнтів могли мати даний товар у кошику одночасно
  localStorage.orders = JSON.stringify(allOrders);
  location.reload();
}

orderBox.addEventListener("click", (e) => {
  let btn = e.target;
  if (btn.tagName === "BUTTON") deleteOrder(btn.dataset.id);
});

//Відкриваємо сторінку товару при натисканні на фото

orderBox.addEventListener("click", (e) => {
  if (e.target.closest(".order-img")) {
    let item = e.target;
    const itemId = item.dataset.id;
    window.localStorage.goodsItemId = JSON.stringify(itemId);
    location.href = "../goods/goods.html";
  }
})

//Функція створює масив замовлених товарів, добавляючи до об'єкту звичайного 
//товару значення ключа client
function createMyOrdersConfirmArray() {
  let myOrdersConfirmArray = myOrdersArray.map((item) => { return { ...item, "client": data.showUserInHeader() } });
  return myOrdersConfirmArray;
}

//createMyOrdersConfirmArray();

//Зберігаємо замовлення

document.querySelector(".order-confirm-btn").addEventListener("click", () => {
  let temp = JSON.parse(window.localStorage.ordersConfirm);
  let myOrdersConfirmArray = createMyOrdersConfirmArray();
  temp.push(...myOrdersConfirmArray);
  let orders = JSON.parse(window.localStorage.orders);
  let finalOrders = orders.filter((item) => item.client != data.showUserInHeader());
  let ordersWithDate = temp.map((order) => {
    return {
      ...order,
      date: new Date().toLocaleDateString(),
      time: new Date().toLocaleTimeString()
    }
  })
  window.localStorage.orders = JSON.stringify(finalOrders);
  window.localStorage.ordersConfirm = JSON.stringify(ordersWithDate);
  location.reload();
})