import * as data from "../../src/server.js";

data.baseArrayCreate();
data.showUserInHeader();
data.onLoadHeader();
data.numbersOfOrders();
data.numbersWorkingOrders();

document.querySelector(".header-left").addEventListener("mouseover", data.toolTipMainMenu);
document.querySelector(".header-right-center").addEventListener("click", data.showOrder);  
document.querySelector(".header-left-center").addEventListener("click", data.showWorkingOrders);
document.querySelector(".header-right").addEventListener("click", data.showEditForm);

function queryDiv(selector) {
  return document.querySelector(selector);
};

queryDiv(".main-box>div:nth-child(1)").addEventListener("click", () => {
  location.href = "../shop/shop.html";
});
queryDiv(".main-box>div:nth-child(2)").addEventListener("click", () => {
  location.href = "../historyOrders/historyOrders.html";
});
queryDiv(".main-box>div:nth-child(3)").addEventListener("click", () => {
  location.href = "../purchase/purchase.html";
});
queryDiv(".main-box>div:nth-child(4)").addEventListener("click", () => {
  location.href = "../comments/comments.html";
})